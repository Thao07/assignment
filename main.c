#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <fcntl.h> //_O_U16TEXT
#include <wchar.h>

void taoMenu() {
    printf("------------->MENU<--------------\n");
    printf("Vui lòng nhập lựa chọn của bạn:\n");
    printf("1. Thêm mới sinh.\n");
    printf("2. Hiển thị danh sách sinh viên.\n");
    printf("3. Lưu danh sách sinh viên ra file.\n");
    printf("4. Đọc danh sách sinh viên từ file.\n");
    printf("5. Thoát chương trình.\n");
    printf("---------------------------------\n");
}

int n;
int i;
typedef struct {
    char msv[6];
    char name[50];
    char phone[50];
} Student;
Student listStudent[10];

void removeStdChar(char array[]) {
    if (strchr(array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}

void listSV(Student listStudent[n]) {
    for (int i = 0; i < n; ++i) {
        while (1) {
            printf("Vui lòng nhập mã sinh viên thứ %d:\n", i + 1);
            scanf("%s", listStudent[i].msv);
            if (strlen(listStudent[i].msv) != 5) {
                printf("Mã sinh viên không đủ 5 kí tự. Vui lòng nhập lại mã Sinh viên.\n ");
            } else {
                break;
            }
        }
        //listStudent[i].msv[strlen(listStudent[i].msv)-1] = ' ';
        removeStdChar(listStudent[i].msv);
        printf("Vui lòng nhập tên sinh viên thứ %d:", i + 1);
        fgets(listStudent[i].name, sizeof(listStudent[i].name) * sizeof(char), stdin);
        removeStdChar(listStudent[i].name);
        listStudent[i].name[strlen(listStudent[i].name) - 1] = ' ';
        printf("Vui lòng nhập số điện thoại sinh viên thứ %d:", i + 1);
        fgets(listStudent[i].phone, sizeof(listStudent[i].phone) * sizeof(char), stdin);
        removeStdChar(listStudent[i].phone);
        listStudent[i].phone[strlen(listStudent[i].phone) - 1] = ' ';
    }
}

void displaylistStudent(Student SV[]) {
    printf("-%-20s  |          %-22s  |  %-30s \n", "Mã Sinh Viên", "Tên Sinh Viên", "Số Điện Thoại");
    for (int i = 0; i < n; ++i) {
        printf("-%-20s|%-32s|  %-30s \n", listStudent[i].msv, listStudent[i].name, listStudent[i].phone);
    }
}

int writelistStudent() {
    FILE *fp;
    fp = fopen("../danhsachsinhvien.txt", "w+");
    fprintf(fp, "-%-20s  |          %-22s  |  %-30s \n", "Mã Sinh Viên", "Tên Sinh Viên", "Số Điện Thoại");
    for (int i = 0; i < n; ++i) {
        fprintf(fp, "-%-20s|%-32s|  %-30s \n", listStudent[i].msv, listStudent[i].name, listStudent[i].phone);
    }
    printf("lưu danh sách thành công.\n");
    fclose(fp);
    return writelistStudent;
}
void readFromFile() {
    FILE *fp;
    fp = fopen("../danhsachsinhvien.txt", "r+");
    if (fp == NULL) {
        printf("Danh sách sinh viên chưa có thông tin nào.\n");
    }
    while (fgets(listStudent, sizeof(listStudent)* sizeof(char), fp) != NULL){
        printf("%s", listStudent);
    }
}
int main() {
    while (1) {
        taoMenu();
        int choice;
        scanf("%d", &choice);
        switch (choice) {
            case 1:
                do {
                    printf("Vui lòng nhập số sinh viên: ");
                    scanf("%d", &n);
                    if (n <= 0 || n > 10) {
                        printf("Số sinh viên trong danh sách không thể bằng 0 hoặc lớn hơn 10.\n");
                    }
                } while (n <= 0 || n > 10);
                listSV(listStudent);
                if (n == 10) {
                    printf("Số sinh viên trong danh sách đã đủ 10 sinh viên.\n");
                    printf("Vui lòng xem lại menu và chọn một lựa chọn khác.\n");
                }
                break;
            case 2:
                displaylistStudent(listStudent);
                break;
            case 3:
                writelistStudent();
                break;
            case 4:
                readFromFile();
                break;
            case 5:
                exit(1);
        }
    }
    return 0;
}
